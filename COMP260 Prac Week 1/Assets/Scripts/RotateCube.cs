﻿using UnityEngine;
using System.Collections;

public class RotateCube : MonoBehaviour
{

	// a public parameter that can be set in the Inspector
	public float turnSpeed = 180.0f;	// 180 degrees per second

	// a private state variable that is hidden from the Inspector
	private bool clockwise;


	void Start()
    {
		// this code is run once when the script is first enabled 
		// (usually at the beginning of the scene)

		// initialise the private state
 		clockwise = true;
	}

	void Update ()
    {
		// this code is called on every frame (~30-60 times per second)

		// compute how much to turn it in this frame		
		float angle = turnSpeed * Time.deltaTime;

		if (!clockwise)
        {
			angle = -angle;
		}

		// rotate the cube about the Y-axis
		transform.Rotate(0, angle, 0);
	}

	void OnMouseDown()
    {
		// this code is called when the object is clicked on

		// reverse the direction
		clockwise = !clockwise;
	}
}
